README:

1) 	download all files

2) 	within "robotics_lab.launch" edit the "default" argument within this line

		<arg name="world_file"  default="/home/bpm/DevWorkspace/ROS/worlds/robotics_lab.world"/>

	to the location where the .world file has been saved, the .world file can be saved anywhere you want

3) 	copy "robotics_lab.launch" into "/opt/ros/indigo/share/turtlebot_gazebo/launch/"
	
		NOTE: you will most likely need to use "sudo cp" rather than just "cp"

4) 	copy the entire "RoboCup_SPL_Ball_Large" folder to "~/.gazebo/models" ...this should not require sudo permissions


- You should be able to run "roslaunch turtlebot_gazebo robotics_lab.launch" from terminal
- The makeshift lab should populate with the red ball already present in the room
- If the red ball doesn't immediately show up, you can insert it from the "insert" tab in gazebo
	


